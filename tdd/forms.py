from django import forms

class Status_Form(forms.Form):
	error_messages = {
	'required':'kabar saya ...',
	'invalid':'tidak valid',
	}

	attrs = {
	'class':'form-control'
	}

	form = forms.CharField(label='status ', required=True, max_length=300,
		widget=forms.TextInput(attrs=attrs))

