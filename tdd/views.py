from django.shortcuts import render, redirect
from .models import Form
from .forms import Status_Form

# Create your views here.
def index(request):
    form = Form.objects.all()
    fillForm = Status_Form
    return render(request, 'index.html', {'Form': form, 'fillForm': fillForm})

def profil(request):
    return render(request, 'profil.html', {})

def tambah_form(request):
	form = request.POST["form"]
	buat_form = Form(form = form)
	buat_form.save()
	return redirect(index)

def hapus_form(request):
	form = Form.objects.all().delete()
	return redirect(index)

