from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, tambah_form, hapus_form, profil
from .models import Form
from .forms import Status_Form

# Create your tests here.
class Lab6UnitTest(TestCase):
 
    def test_lab_6_url_is_exist(self):
        response = Client().get('/tdd/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = Form.objects.create(form = 'aku stress')
        counting_all_available_status = Form.objects.all().count()
        self.assertEqual(counting_all_available_status,1)

    def test_hello_message(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', html_response) 

    def test_name_message(self):
        request = HttpRequest()
        response = profil(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Windu Andira', html_response)