"""lab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, re_path, include
from django.conf.urls import url
from tdd import views
from django.contrib import admin
from tdd.views import *

urlpatterns = [
	path('admin/', admin.site.urls),
    path('', include('tdd.urls')),
    path('index/', index, name='index'),
    path('profil', views.profil, name='profil'),
    url('', index, name='index'),
    url(r'^tambah_form/$', tambah_form, name="tambah_form"),
    url(r'^hapus_form/$', hapus_form, name="hapus_form"),
]    
